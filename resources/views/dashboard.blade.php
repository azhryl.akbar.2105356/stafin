@extends('layouts.template')

@section('tabTitle', 'Dashboard')
@section('title', 'Hello Stafin')
@section('dashboard', 'cl0')

@section('left')
    <section>
        <div class="d-flex">
            <img src="{{ asset('img/in.png') }}" alt="">
            <div class="ps-4 w-100">
                <div class="d-flex justify-content-between">
                    <p class="f400 fs-6 wht9">Earnings</p>
                    <H6 class="f600 fs-5 wht9">Rp 1.000.000</H6>
                </div>
                <x-progress color="#92DCFE" percentage="60"></x-progress>
            </div>
        </div>
        <br>
        <div class="d-flex">
            <img src="{{ asset('img/out.png') }}" alt="">
            <div class="ps-4 w-100">
                <div class="d-flex justify-content-between">
                    <p class="f400 fs-6 wht9">Spendings</p>
                    <H6 class="f600 fs-5 wht9">Rp 900.000</H6>
                </div>
                <x-progress color="#ED5333" percentage="85"></x-progress>
            </div>
        </div>
    </section>
@endsection

@section('left1')
    <x-card :shadow="true" style="primary">
        <h2 class="ps-4 py-2 mb-4 fs-4 wht9 f600">Last Transaction</h2>
        <x-card :shadow="false" style="secondary">
            <div class="d-flex justify-content-between">
                <div class="d-flex">
                    <span class="align-self-center bi bi-arrow-down-circle fs-2 cl0"></span>
                    <div class="align-self-center ps-3">
                        <p class="f600 fs-6 wht9 mb-0 ">Breakfast</p>
                        <p class="f400 fs-7 wht6 mb-0 lt0">24 Mei 2022 at 10.20</p>
                    </div>
                </div>
                <p class="align-self-center m-0 f600 fs-5 wht9">Rp 100.000</p>
            </div>
        </x-card>
        <x-card :shadow="false" style="secondary">
            <div class="d-flex justify-content-between">
                <div class="d-flex">
                    <span class="align-self-center bi bi-arrow-up-circle fs-2 cl1"></span>
                    <div class="align-self-center ps-3">
                        <p class="f600 fs-6 wht9 mb-0 ">Breakfast</p>
                        <p class="f400 fs-7 wht6 mb-0 lt0">24 Apr 2022 at 10.20</p>
                    </div>
                </div>
                <p class="align-self-center m-0 f600 fs-5 wht9">Rp 100.000</p>
            </div>
        </x-card>
        <x-card :shadow="false" style="secondary">
            <div class="d-flex justify-content-between">
                <div class="d-flex">
                    <span class="align-self-center bi bi-arrow-down-circle fs-2 cl0"></span>
                    <div class="align-self-center ps-3">
                        <p class="f600 fs-6 wht9 mb-0 ">Breakfast</p>
                        <p class="f400 fs-7 wht6 mb-0 lt0">24 Apr 2022 at 10.20</p>
                    </div>
                </div>
                <p class="align-self-center m-0 f600 fs-5 wht9">Rp 100.000</p>
            </div>
        </x-card>
    </x-card>
@endsection

@section('right')
    <x-card :shadow="true" style="primary">
        <h2 class="ps-4 py-2 mb-4 fs-4 wht9 f600">Today's Task</h2>
        <section>
            <x-card :shadow="true" style="secondary">
                <div class="d-flex justify-content-between py-2">
                    <div class="d-flex">
                        <div class="align-self-center rounded-circle me-3"
                            style="width:1.25rem; height:1.25rem; border:3px solid #ED5333"></div>
                        <p class="align-self-center m-0 f600 fs-5 wht9">Homework</p>
                    </div>
                    <p class="align-self-center m-0 f400 fs-6 wht8">18.20</p>
                </div>
            </x-card>
            <x-card :shadow="true" style="secondary">
                <div class="d-flex justify-content-between py-2">
                    <div class="d-flex">
                        <div class="align-self-center rounded-circle me-3"
                            style="width:1.25rem; height:1.25rem; border:3px solid #92DCFE"></div>
                        <p class="align-self-center m-0 f600 fs-5 wht9">Homework</p>
                    </div>
                    <p class="align-self-center m-0 f400 fs-6 wht8">18.20</p>
                </div>
            </x-card>
            <x-card :shadow="true" style="secondary">
                <div class="d-flex justify-content-between py-2">
                    <div class="d-flex">
                        <div class="align-self-center rounded-circle me-3"
                            style="width:1.25rem; height:1.25rem; border:3px solid #92DCFE"></div>
                        <p class="align-self-center m-0 f600 fs-5 wht9">Homework</p>
                    </div>
                    <p class="align-self-center m-0 f400 fs-6 wht8">18.20</p>
                </div>
            </x-card>
        </section>
        <p class="ps-4 pt-3 pb-0 fs-6 wht8 f600">Upcoming</p>
        <section>
            <x-card :shadow="false" style="secondary">
                <div class="d-flex justify-content-between p-1">
                    <p class="align-self-center m-0 f600 fs-6 wht6">Homework</p>
                    <p class="align-self-center m-0 f400 fs-6 wht6">24 Apr</p>
                </div>
            </x-card>
            <x-card :shadow="false" style="secondary">
                <div class="d-flex justify-content-between p-1">
                    <p class="align-self-center m-0 f600 fs-6 wht6">Homework</p>
                    <p class="align-self-center m-0 f400 fs-6 wht6">24 Apr</p>
                </div>
            </x-card>
            <x-card :shadow="false" style="secondary">
                <div class="d-flex justify-content-between p-1">
                    <p class="align-self-center m-0 f600 fs-6 wht6">Homework</p>
                    <p class="align-self-center m-0 f400 fs-6 wht6">24 Apr</p>
                </div>
            </x-card>
        </section>
    </x-card>
@endsection

@section('bottom')
    <x-card :shadow="true" style="primary">
        <h2 class="ps-4 ms-3 py-2 mb-2 fs-4 wht9 f600">Upcoming Event</h2>
        <section class="d-flex" style="overflow-x:scroll; scrolbar">
            <x-card class="me-4" :shadow="true" style="secondary">
                <div class="px-3 py-4">
                    <div class="d-flex">
                        <div class="align-self-center rounded-circle me-3"
                            style="width:1rem; height:1rem; border:3px solid #ED5333"></div>
                        <p class="align-self-center m-0 f400 fs-7 wht6">Vote Uncompleted</p>
                    </div>
                    <div class="pt-3" style="width: 300px">
                        <p class="fs-5 wht9 f600 mb-0">
                            Class Meeting
                        </p>
                        <p class="fs-7 wht6 f400 text">
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Veritatis dolorum id
                            dignissimos quod voluptatem, quam repellendus magni nemo. Accusantium ex
                            reiciendis
                            voluptatem nemo dicta molestiae ut iure nihil omnis cumque!
                        </p>
                        <div class="d-flex justify-content-end">
                            <p class="mb-0 fs-7 wht9 f600 ">24 Mei 2022 at 10.20</p>
                        </div>
                    </div>
                </div>
            </x-card>
            <x-card class="me-4" :shadow="true" style="secondary">
                <div class="px-3 py-4">
                    <div class="d-flex">
                        <div class="align-self-center rounded-circle me-3"
                            style="width:1rem; height:1rem; border:3px solid #ED5333"></div>
                        <p class="align-self-center m-0 f400 fs-7 wht6">Vote Uncompleted</p>
                    </div>
                    <div class="pt-3" style="width: 300px">
                        <p class="fs-5 wht9 f600 mb-0">
                            Class Meeting
                        </p>
                        <p class="fs-7 wht6 f400 text">
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Veritatis dolorum id
                            dignissimos quod voluptatem, quam repellendus magni nemo. Accusantium ex
                            reiciendis
                            voluptatem nemo dicta molestiae ut iure nihil omnis cumque!
                        </p>
                        <div class="d-flex justify-content-end">
                            <p class="mb-0 fs-7 wht9 f600 ">24 Mei 2022 at 10.20</p>
                        </div>
                    </div>
                </div>
            </x-card>
            <x-card class="me-4" :shadow="true" style="secondary">
                <div class="px-3 py-4">
                    <div class="d-flex">
                        <div class="align-self-center rounded-circle me-3"
                            style="width:1rem; height:1rem; border:3px solid #ED5333"></div>
                        <p class="align-self-center m-0 f400 fs-7 wht6">Vote Uncompleted</p>
                    </div>
                    <div class="pt-3" style="width: 300px">
                        <p class="fs-5 wht9 f600 mb-0">
                            Class Meeting
                        </p>
                        <p class="fs-7 wht6 f400 text">
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Veritatis dolorum id
                            dignissimos quod voluptatem, quam repellendus magni nemo. Accusantium ex
                            reiciendis
                            voluptatem nemo dicta molestiae ut iure nihil omnis cumque!
                        </p>
                        <div class="d-flex justify-content-end">
                            <p class="mb-0 fs-7 wht9 f600 ">24 Mei 2022 at 10.20</p>
                        </div>
                    </div>
                </div>
            </x-card>
        </section>
    </x-card>
@endsection
