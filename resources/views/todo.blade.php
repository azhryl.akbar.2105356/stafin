@extends('layouts.template')

@section('tabTitle', 'To Do')
@section('title', 'To Do')
@section('todo', 'cl0')

@section('left')
@endsection

@section('left1')
    <x-card :shadow="true" style="primary">
        <h2 class="ps-4 py-2 mb-4 fs-4 wht9 f600">New To Do</h2>
        <div>
            <div class="d-flex mb-2">
                <input type="text" style="outline:none" class="border-0 r20 bg2 px-4 py-3 me-4 fs-5 wht9 f600 w-100"
                    placeholder="Title">
                <button type="button" class="f600 bg2 border-0 r20 wht6 px-4 py-3 ">Time</button>
                <button type="button" class="f600 bg2 border-0 r20 wht6 ms-3 px-4 py-3 ">Date</button>
            </div>
            <textarea class="border-0 bg2 r20 my-4 p-4 fs-6 wht8 f600 w-100" style="outline:none" rows="3"
                placeholder="Description"></textarea>
            <div class="d-flex mb-3">
                <button type="button" class="f600 shdw bg2 border-0 r20 wht9 px-5 py-3 ">Add To Do</button>
                <button type="button" class="f600 bg-transparent border-0 r20 wht6 px-4 py-3 ">Clear</button>
            </div>
        </div>
    </x-card>
    <section class="pt-5 ">
        <h2 class="ps-5 py-2 mb-4 fs-4 wht9 f600">Uncompleted Tasks</h2>
        <x-card :shadow="true" style="secondary" class="my-4">
            <div class="px-3 d-flex justify-content-between">
                <div class="d-flex">
                    <div class="align-self-center rounded-circle me-3"
                        style="width:1.25rem; height:1.25rem; border:3px solid #ED5333"></div>
                    <div class="align-self-center ps-2">
                        <p class="f600 fs-6 wht9 mb-0 ">Breakfast</p>
                        <p class="f400 fs-7 wht6 mb-0 lt0">Description</p>
                    </div>
                </div>
                <p class="align-self-center m-0 f600 fs-6 wht9">24 Mei 2022 at 10.20</p>
            </div>
        </x-card>
        <x-card :shadow="true" style="secondary" class="my-4">
            <div class="px-3 d-flex justify-content-between">
                <div class="d-flex">
                    <div class="align-self-center rounded-circle me-3"
                        style="width:1.25rem; height:1.25rem; border:3px solid #ED5333"></div>
                    <div class="align-self-center ps-2">
                        <p class="f600 fs-6 wht9 mb-0 ">Breakfast</p>
                        <p class="f400 fs-7 wht6 mb-0 lt0">Description</p>
                    </div>
                </div>
                <p class="align-self-center m-0 f600 fs-6 wht9">24 Apr 2022 at 10.20</p>
            </div>
        </x-card>
        <x-card :shadow="true" style="secondary" class="my-4">
            <div class="px-3 d-flex justify-content-between">
                <div class="d-flex">
                    <div class="align-self-center rounded-circle me-3"
                        style="width:1.25rem; height:1.25rem; border:3px solid #ED5333"></div>
                    <div class="align-self-center ps-2">
                        <p class="f600 fs-6 wht9 mb-0 ">Breakfast</p>
                        <p class="f400 fs-7 wht6 mb-0 lt0">Description</p>
                    </div>
                </div>
                <p class="align-self-center m-0 f600 fs-6 wht9">24 Apr 2022 at 10.20</p>
            </div>
        </x-card>
    </section>
@endsection

@section('right')
    <section class="mt-5">
        <h2 class="ps-4 py-2 mb-4 fs-4 wht9 f600">Today's Tasks</h2>
        <section>
            <x-card :shadow="true" style="secondary">
                <div class="d-flex justify-content-between py-2">
                    <div class="d-flex">
                        <div class="align-self-center rounded-circle me-3"
                            style="width:1.25rem; height:1.25rem; border:3px solid #ED5333"></div>
                        <p class="align-self-center m-0 f600 fs-5 wht9">Homework</p>
                    </div>
                    <p class="align-self-center m-0 f400 fs-6 wht8">18.20</p>
                </div>
            </x-card>
            <x-card :shadow="true" style="secondary">
                <div class="d-flex justify-content-between py-2">
                    <div class="d-flex">
                        <div class="align-self-center rounded-circle me-3"
                            style="width:1.25rem; height:1.25rem; border:3px solid #ED5333"></div>
                        <p class="align-self-center m-0 f600 fs-5 wht9">Homework</p>
                    </div>
                    <p class="align-self-center m-0 f400 fs-6 wht8">18.20</p>
                </div>
            </x-card>
            <x-card :shadow="true" style="secondary">
                <div class="d-flex justify-content-between py-2">
                    <div class="d-flex">
                        <div class="align-self-center rounded-circle me-3"
                            style="width:1.25rem; height:1.25rem; border:3px solid #ED5333"></div>
                        <p class="align-self-center m-0 f600 fs-5 wht9">Homework</p>
                    </div>
                    <p class="align-self-center m-0 f400 fs-6 wht8">18.20</p>
                </div>
            </x-card>
        </section>
        <p class="ps-4 pt-5 pb-0 fs-6 wht8 f600">Completed Tasks</p>
        <section>
            <x-card :shadow="false" style="secondary">
                <div class="d-flex justify-content-between py-2">
                    <div class="d-flex">
                        <div class="align-self-center rounded-circle me-3"
                            style="width:1.25rem; height:1.25rem; border:3px solid #92DCFE"></div>
                        <p class="align-self-center m-0 f600 fs-5 wht9">Homework</p>
                    </div>
                    <p class="align-self-center m-0 f400 fs-6 wht8">18.20</p>
                </div>
            </x-card>
            <x-card :shadow="false" style="secondary">
                <div class="d-flex justify-content-between py-2">
                    <div class="d-flex">
                        <div class="align-self-center rounded-circle me-3"
                            style="width:1.25rem; height:1.25rem; border:3px solid #92DCFE"></div>
                        <p class="align-self-center m-0 f600 fs-5 wht9">Homework</p>
                    </div>
                    <p class="align-self-center m-0 f400 fs-6 wht8">18.20</p>
                </div>
            </x-card>
            <x-card :shadow="false" style="secondary">
                <div class="d-flex justify-content-between py-2">
                    <div class="d-flex">
                        <div class="align-self-center rounded-circle me-3"
                            style="width:1.25rem; height:1.25rem; border:3px solid #92DCFE"></div>
                        <p class="align-self-center m-0 f600 fs-5 wht9">Homework</p>
                    </div>
                    <p class="align-self-center m-0 f400 fs-6 wht8">18.20</p>
                </div>
            </x-card>
        </section>
    </section>
@endsection

@section('bottom')
@endsection
