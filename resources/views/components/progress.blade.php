<div>
    <div class="rounded-pill float-start"
        style="height: 10px; width: {{ $percentage }}%; background-color: {{ $color }}">
    </div>
    <div class="rounded-pill w-100 bg2" style="height: 10px;"></div>
</div>
